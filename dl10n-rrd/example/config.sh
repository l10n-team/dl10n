# Location were the generated files are written (for each suite, the RRD
# files are written in the po, po4a, podebconf, and man subdirectories;
# the graphs are written in man.png and in the week, month and year
# subdirectories).
RRD_HOME=/srv/i18n.debian.net/www/debian-l10n-stats

# Location of the debian-l10n working directory
DL10N_HOME=/srv/dl10n-stuff/svn/dl10n/

