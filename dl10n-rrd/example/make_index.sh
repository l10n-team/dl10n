#!/bin/sh

export LC_ALL=C

echo "
<html>
  <body>
    <table>
      <tr align=center>
        <td align=right><b>language</b></td>
        <td><b>PO</b></td>
        <td><b>po4a</b></td>
        <td><b>po-debconf</b></td>
      </tr>
      <tr align=center>
        <td align=right><i>ranks</i></td>
        <td><a href=po.png>po.png</a></td>
        <td><a href=po4a.png>po4a.png</a></td>
        <td><a href=podebconf.png>podebconf.png</a></td>
      </tr>" > index.html

for lang in po-*.png
do
    lang=${lang%.png}
    lang=${lang#po-}
    if [ $lang = "_" ] || [ $lang = "__" ] || [ $lang = "___" ]
    then
        continue
    fi
    echo "
      <tr align=center>
        <td align=right>$lang</td>" >> index.html
    for f in po po4a podebconf
    do
        echo "
        <td>" >> index.html
        if [ -f $f-$lang.png ]
        then
            echo "
          <a href=$f-$lang.png>$f-$lang.png</a>" >> index.html
        else
            echo "
          -" >> index.html
        fi
        echo "
        </td>" >> index.html
    done
    echo "
      </tr>" >> index.html
done

echo "
    </table>
    <br/>
    These statistics are done with <a href="../podebconf">these</a> data.
    You can also find the scripts generating these graph and data
    <a href="../..">there</a>.
    The are also RRD files for the <a href="../po4a">po4a strings</a> and
    <a href="../po">every PO's strings</a>.
    An <a href="../../unstable/$period">unstable</a>,
    <a href="../../unstableBTS/$period">unstable with BTS</a> and
    <a href="../../testing/$period">testing</a>
    version of this page exists.
    <br/>
    In this graphs, the <i>no po</i> and untranslated categories differ. The
    untranslated category indicate the number of strings which are not
    translated in the distributed PO files. The <i>no po</i> category is an
    estimation of the number of strings which are not translated because there
    is no PO files for the given language. This estimation is correct for the
    <i>po4a</i> and <i>podebconf</i> famillies, but is just an approximative
    number for the <i>po</i> familly because of missing POT files (in that
    case, the biggest PO file is considered).
  </body>
</html>" >> index.html

